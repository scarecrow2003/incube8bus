const LOAD_BUS = 'incube8-bus/bus/LOAD_BUS';
const LOAD_BUS_SUCCESS = 'incube8-bus/bus/LOAD_BUS_SUCCESS';
const LOAD_BUS_FAIL = 'incube8-bus/bus/LOAD_BUS_FAIL';
const LOAD_STOP = 'incube8-bus/bus/LOAD_STOP';
const LOAD_STOP_SUCCESS = 'incube8-bus/bus/LOAD_STOP_SUCCESS';
const LOAD_STOP_FAIL = 'incube8-bus/bus/LOAD_STOP_FAIL';
const SAVE_BUS = 'incube8-bus/bus/SAVE_BUS';
const SAVE_BUS_SUCCESS = 'incube8-bus/bus/SAVE_BUS_SUCCESS';
const SAVE_BUS_FAIL = 'incube8-bus/bus/SAVE_BUS_FAIL';

const initialState = {
  stops: [],
  buses: {}
};
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD_STOP_SUCCESS:
      return {
        ...state,
        stops: action.result
      };
    case LOAD_BUS_SUCCESS:
      return {
        ...state,
        buses: Object.assign({}, state.buses, action.result)
      };
    case SAVE_BUS_SUCCESS:
      const buses = Object.assign({}, state.buses);
      if (buses[Object.keys(action.result)[0]]) {
        buses[Object.keys(action.result)[0]].push(action.result[Object.keys(action.result)[0]]);
      } else {
        buses[Object.keys(action.result)[0]] = new Array(action.result[Object.keys(action.result)[0]]);
      }
      return {
        ...state,
        buses: buses
      };
    default:
      return state;
  }
}

export function loadBuses(position) {
  return {
    types: [LOAD_BUS, LOAD_BUS_SUCCESS, LOAD_BUS_FAIL],
    promise: (client) => client.post('/bus', {data: {position: position}})
  };
}

export function loadStops(position) {
  return {
    types: [LOAD_STOP, LOAD_STOP_SUCCESS, LOAD_STOP_FAIL],
    promise: (client) => client.post('/stop', {data: {position: position}})
  };
}

export function saveBus(busNumber, busStop) {
  return {
    types: [SAVE_BUS, SAVE_BUS_SUCCESS, SAVE_BUS_FAIL],
    promise: (client) => client.post('/savebus', {
      data: {
        bus_number: busNumber,
        bus_stop: busStop
      }
    })
  };
}
