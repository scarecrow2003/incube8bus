import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import * as authActions from 'redux/modules/auth';
import * as busActions from 'redux/modules/bus';
import {BusForm} from 'components';

@connect(
    state => ({user: state.auth.user, stops: state.bus.stops, buses: state.bus.buses}),
    Object.assign({}, authActions, busActions))
export default
class Bus extends Component {
  static propTypes = {
    user: PropTypes.object,
    stops: PropTypes.array,
    buses: PropTypes.object,
    logout: PropTypes.func,
    loadStops: PropTypes.func,
    loadBuses: PropTypes.func,
    saveBus: PropTypes.func
  }

  loadBusStops = (event) => {
    event.preventDefault();
    this.props.loadStops(this.props.user.position);
  }

  loadBuses(position) {
    this.props.loadBuses(position);
  }

  render() {
    const {user, logout} = this.props;
    return (user &&
      <div className="container">
        <h1>Login Success</h1>

        <div>
          <p>Hi, {user.name}. You have just successfully logged in. <button className="btn btn-danger" onClick={logout}><i className="fa fa-sign-out"/>{' '}Log Out</button></p>
          <p>Assume you are in a city of 10 x 10. You are currently at position [{user.position[0]}, {user.position[1]}].
            {this.props.stops.length === 0 ? <button className="btn btn-default" onClick={this.loadBusStops}>Show Nearest Bus Stops</button> : null}
          </p>
          <p className={this.props.stops.length > 0 ? '' : 'hidden'}>The nearest stops are:
            {this.props.stops.map(position => {
              return (<div>
                [{position[0]}, {position[1]}] <button className="btn btn-default" onClick={this.loadBuses.bind(this, position)}>Show Buses</button>
                {this.props.buses[position] ? <div>
                  {this.props.buses[position].map(bus => {
                    const key = Object.keys(bus)[0];
                    const val = bus[key];
                    return (<div>
                      Bus {key} will arrive in {val} minute{val > 1 ? 's' : ''}.
                    </div>);
                  })}
                </div> : null}
              </div>);
            })}
          </p>
          {this.props.stops.length > 0 ? <BusForm stops={this.props.stops} saveBus={this.props.saveBus}/> : null}
        </div>
      </div>
    );
  }
}
