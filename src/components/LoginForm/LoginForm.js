import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import loginValidation from './loginValidation';

@reduxForm({
  form: 'loginForm',
  fields: ['username', 'password'],
  validate: loginValidation
})
export default class LoginForm extends Component {
  static propTypes = {
    fields: PropTypes.object.isRequired,
    username: PropTypes.string,
    password: PropTypes.string,
    invalid: PropTypes.bool.isRequired,
    login: PropTypes.func
  }

  handleSubmit = (event) => {
    event.preventDefault();
    this.props.login(this.username.value, this.password.value);
    this.username.value = '';
  }

  render() {
    const {fields: {username, password}, invalid} = this.props;
    return (
      <form className="login-form form-inline" onSubmit={this.handleSubmit}>
        <div className="form-group col-xs-12 col-md-3">
          <input type="text" ref={input => this.username = input} placeholder="Enter username" className="form-control" {...username}/>
          {username.error && username.touched && <div className="text-danger">{username.error}</div>}
        </div>
        <div className="form-group col-xs-12 col-md-3">
          <input type="password" ref={input => this.password = input} placeholder="Enter password" className="form-control" {...password}/>
          {password.error && password.touched && <div className="text-danger">{password.error}</div>}
        </div>
        <button className="btn btn-success" onClick={this.handleSubmit} disabled={invalid}><i className="fa fa-sign-in"/>{' '}Log In
        </button>
      </form>
    );
  }
}
