import {createValidator, required, minLength, email} from 'utils/validation';

const busValidation = createValidator({
  username: [required, email, minLength(3)],
  password: [required, minLength(8)]
});
export default busValidation;
