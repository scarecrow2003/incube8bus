import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import busValidation from './busValidation';

@reduxForm({
  form: 'busForm',
  fields: ['stop', 'bus'],
  validate: busValidation
})
export default class BusForm extends Component {
  static propTypes = {
    saveBus: PropTypes.func,
    fields: PropTypes.object.isRequired,
    stop: PropTypes.array,
    bus: PropTypes.integer,
    stops: PropTypes.array,
    invalid: PropTypes.bool.isRequired
  }

  handleSave = (event) => {
    event.preventDefault();
    this.props.saveBus(this.busNumber.value, this.busStop.value);
  };

  render() {
    const {fields: {stop, bus}, invalid} = this.props;
    return (
      <form>
        <h2>Register Bus</h2>
        <div className="form-group">
          <label>Bus Number</label>
          <input type="text" className="form-control" ref={input => this.busNumber = input} {...bus}/>
          {bus.error && bus.touched && <div className="text-danger">{bus.error}</div>}
        </div>
        <div className="form-group">
          <label>Bus Stop</label>
          <select className="form-control" ref={input => this.busStop = input} {...stop}>
                {this.props.stops.map(onestop => <option value={onestop} key={onestop}>[{onestop[0]}, {onestop[1]}]</option>)}
          </select>
        </div>
        <button className="btn btn-success" onClick={this.handleSave} disabled={invalid}>Save</button>
      </form>
    );
  }
}
