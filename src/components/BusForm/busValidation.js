import {createValidator, required, integer} from 'utils/validation';

const busValidation = createValidator({
  bus: [required, integer]
});
export default busValidation;
