require('babel-polyfill');

const environment = {
  development: {
    isProduction: false
  },
  production: {
    isProduction: true
  }
}[process.env.NODE_ENV || 'development'];

module.exports = Object.assign({
  host: process.env.HOST || 'localhost',
  port: process.env.PORT,
  apiHost: process.env.APIHOST || 'localhost',
  apiPort: process.env.APIPORT,
  app: {
    title: 'Incube8 bus',
    description: 'Incube8 bus example.',
    head: {
      titleTemplate: 'Incube8 bus: %s',
      meta: [
        {name: 'description', content: 'Incube8 bus.'},
        {charset: 'utf-8'},
        {property: 'og:site_name', content: 'Incube8 bus'},
        {property: 'og:image', content: 'https://www.google.com/logo.jpg'},
        {property: 'og:locale', content: 'en_US'},
        {property: 'og:title', content: 'Incube8 bus'},
        {property: 'og:description', content: 'Incube8 bus.'},
        {property: 'og:card', content: 'summary'},
        {property: 'og:site', content: ''},
        {property: 'og:creator', content: ''},
        {property: 'og:image:width', content: '200'},
        {property: 'og:image:height', content: '200'}
      ]
    }
  }
}, environment);
