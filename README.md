This document contains information about how to bootstrap the project and get started quickly with the configurations.

**Getting Started**

1. Run "npm install" to install the necessary packages.

2. Run "npm run dev" to start the project.

3. Browse to http://localhost:3000 to go to home page of the project.
You need to install Redux DevTools Extension for your Chrome to correctly view the pages.

4. Click login to go to the login page.

5. Enter username and password to login.
Validation is enabled. Username must be email and password must be at least 8 characters.
As I don't have enough time to code the backend, I just use Express as the backend.
As you pass the validation, the backend will return whatever username you entered.
The backend will also return a random coordinate as the position of the user.

6. Assume you are in a city of 10 x 10 positions. Click the "Show Nearest Bus Stops" will call the backend to get the nearest bus stops.
Again, the backend randomly generate 5 coordinates as the positions of the nearest bus stop.

7. Click "Show Buses" to show buses of that bus stop and bus arriving time.
The backend will randomly generate some bus information for the stop.

8. Once you did step 6, the "Register Bus" section will go and you will be able to register a bus to a bus stop.
Under "Bus Number", enter a bus number. It must be integer with validation.
The "Bus Stop" is a dropdown selector and lists the above bus stops for you to select.
Once the "Bus Number" is entered, the "Save" button will be enabled and you can click to save and the bus will be added to the bus stop.

9. You can click "Log Out" button to log out of the system.