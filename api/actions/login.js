export default function login(req) {
  const user = {
    name: req.body.name,
    position: [Math.floor(Math.random()*10), Math.floor(Math.random()*10)]
  };
  req.session.user = user;
  return Promise.resolve(user);
}
