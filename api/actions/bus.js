export default function bus(req) {
  let position = req.body.position;
  let buses = [];
  let number = Math.floor(Math.random()*10)+1;
  for (let i=0; i<number; i++) {
    let bus = {};
    bus[Math.floor(Math.random()*100)+1] = Math.floor(Math.random()*5)+1;
    buses.push(bus);
  }
  let result = {};
  result[position] = buses;
  return Promise.resolve(result);
}
