export default function bus(req) {
  let busNumber = req.body.bus_number;
  let busStop = req.body.bus_stop;
  let bus = {};
  bus[busNumber] = Math.floor(Math.random()*5)+1;
  let result = {};
  result[busStop] = bus;
  return Promise.resolve(result);
}
